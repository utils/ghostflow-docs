## Ghostflow Documentation

This repository contains documentation on the design and deployment setup for
`ghostflow`. Its name comes from "Git-HOSTed workFLOW" as it is a tool aimed at
assisting in the implementation of software process workflow actions via a Git
repository hosted in a forge.
