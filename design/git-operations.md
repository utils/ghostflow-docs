## Git operations

The actions performed by `ghostflow` tend to require a lot of work with
Git repositories. However, cloning and updating local working tree state for
every Git operation can be extremely expensive for large repositories or under
I/O contention.

In order to help mitigate this, `ghostflow` uses what we call a "workarea" to
give a place to work on a Git repository while minimizing filesystem resource
usage. This involves using some of Git's "plumbing" to create a directory which
is as empty as possible while also keeping it separate from any other
operations that may be doing work on the repository.

To do this, each workarea is given its own index and empty working tree. First
the index is created from the commit of the repository that is intended to be
worked on. Once this index exists, its internal state is then updated to
believe that then empty working tree is up-to-date.

However, an empty tree is not always useful and some Git mechanisms actually
require an on-disk copy in order to work. One such mechanism is Git's
submodules. To make this work as intended, the `.gitmodules` file is then
checked out into the workarea (if it exists). This involves a bit of a dance
because the non-standard nature of the workarea means that `git checkout`
thinks everything is fine without actually writing the file to disk, so
operations to update the index state are required as well.

Within a workarea, operations which work with the index or working tree are
generally not allowed because they will do things that are not expected given
the setup done. However, it is useful for operations such as creating commits
or merging code. When merging, conflict files are written to the worktree as
normal and can be detected across a broad range of Git versions (Git 2.38 from
October 2022 supports fully in-memory merging via the new `--write-tree` flag
to `git merge-tree`).

The primary operations used throughout `ghostflow` within workareas include
merging and allowing tools to access file contents normally. For merges, there
is additional support for resolving submodule conflicts automatically. This is
done by choosing the side of the conflict which contains the other if possible
(octopus merges are not performed by `ghostflow` and are therefore not
considered). Other operations which require file contents include code format
checking and reformatting.
