## Data management

Data storage in services is a complicated problem as it can easily become a
point of desynchronization between the service and the repository it
corresponds to. In order to mediate this, as much data is stored in the
repository or on the hosting forge as much as possible.

Hidden refs can store repository state or other data, Git attributes can hold
per-file configuration, commit statuses can hold state for whether a commit has
been checked or not, etc.

### Repository data

Storing data within a repository is an easy way to make sure that data is as
backed up as the relevant code. For example, in the `stage` action, the state
of the stage is represented as a hidden ref on the main repository. This allows
`ghostflow` to discover the state of the stage at any time and determine
whether it is up-to-date or not. Since these commits are made by `ghostflow`
and not intended to be stored anywhere in the official history, the commit
message is used to store metadata about the data on the stage itself.

Another use of hidden refs include the `data` action which is designed to work
with CMake's [`ExternalData`][cmake-externaldata] module. For `ghostflow`'s
part of the process, it looks for data pushed to fork repositories in
`refs/data/<algo>/<hash>` refs, downloads and verifies the data object behind
the ref, then uploads it to a shared location (e.g., using `rsync`) if the hash
matches.

[cmake-externaldata]: https://cmake.org/cmake/help/latest/module/ExternalData.html

### File-level data

Per-file information or configuration may be kept as attributes on the files
using `.gitattributes` files. This keeps these files' configuration tracked as
part of history as well and ensures that they do not get out-of-sync.
Attributes are used much more than hidden refs in `ghostflow` for things such
as formatting configuration, size checks, and LFS checking.

For example, a file can be marked as being formatted by a formatter named
"clang-format" using the following:

```gitattributes
*.c             format.clang-format=12 # use `clang-format-12` for all C files
single-file.c   format.clang-format=9  # use `clang-format-9` for this file though
```

### Contribution information

When contributing, there may be more required than the forge provides natively.
Of note, `ghostflow` supports merging a topic into multiple branches at once
whereas most forges only support a single target branch for contributions.
Here, `ghostflow` uses the description of the contribution to contain metadata
for `ghostflow` specifically. The syntax follows Git's "trailer" syntax (see
the [`git-interpret-trailers(1)` docs][git-interpret-trailers] for details)
where key/value are present at the end of a block of text.

[git-interpret-trailers]: https://git-scm.com/docs/git-interpret-trailers

Examples of trailers used by `ghostflow` include:

- `Backport` which goes in a contribution description to indicate that another
  branch should have the contribution merged in.
- `Topic-name` which renames the source branch in merge commits (e.g., in case
  the contribution has a misleading name).
- `Acked-by` (and other `-by` trailers) may be in comments to indicate review
  feedback on a contribution.
- `Do` is used to tell `ghostflow` to perform some explicit action.
