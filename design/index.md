## Design Documentation

This directory contains design documentation for `ghostflow`.

### Design

- [Data storage](data.md)

### Details

- [Git operations](git-operations.md)
